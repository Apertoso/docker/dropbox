FROM debian:buster
ARG DEBIAN_FRONTEND=noninteractive

# Install Python
RUN apt-get update \
    && apt-get install -y \
        python3 \
        python3-gpg \
        wget \
        curl \
        ca-certificates \
        libatomic1 \
        libglib2.0-0 \
        libglapi-mesa \
        libxcb-glx0 \
        libxcb-dri2-0 \
        libxcb-dri3-0 \
        libxcb-present0 \
        libxcb-sync1 \
        libxdamage1 \
        libxshmfence1 \
        libxxf86vm1 \
    # Clean up
    && apt-get clean \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
    && echo OK

RUN mkdir -p /dbox
RUN groupadd -g 999 dropbox
RUN useradd -u 999 -g dropbox -d /dbox dropbox

# Expose the Dropbox directory
VOLUME /dbox

# Setup the Dropbox CLI
ADD https://www.dropbox.com/download?dl=packages/dropbox.py /bin/dropbox.py
RUN chmod 755 /bin/dropbox.py

# Start the Dropbox daemon
ADD run /bin/run
CMD /bin/run

WORKDIR /dbox/Dropbox
